package com.mreil.gradle.testutil.gradlerunner

import com.mreil.gradle.testutil.git.GitHelper
import org.apache.commons.lang3.RandomStringUtils
import spock.lang.Specification

class GradleRunnerTest extends Specification {
    private static final VERSIONS = ["2.2.1", "2.3", "2.4",
                                     "2.5", "2.6", "2.7", "2.8"]

    def "a gradle build file can be executed"() {
        given:
        GradleRunner gr = new GradleRunner("src/main/sampleprojects/simpleproject", version)
                .withGit()

        when:
        String filename = "build/" + RandomStringUtils.randomAlphabetic(10)
        gr.withArguments("-Pfilename=${filename}").run("createfile")

        then:
        new File(filename).exists()
        GitHelper.isValidGitRepo(gr.projectDir)

        where:
        version << VERSIONS
    }

}
