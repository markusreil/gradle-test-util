package com.mreil.gradle.testutil.git

import org.eclipse.jgit.errors.RepositoryNotFoundException
import org.eclipse.jgit.lib.Constants
import spock.lang.Specification

class GitHelperTest extends Specification {
    File dir

    def setup() {
        dir = File.createTempDir()
    }

    def "InitGitAt"() {
        when:
        GitHelper.initGitAt(dir)

        then:
        dir.exists()
        dir.isDirectory()
        GitHelper.isValidGitRepo(dir)
    }

    def "InitGitWithInitialCommitAt"() {
        when:
        def git = GitHelper.initGitWithInitialCommitAt(dir)

        then:
        dir.exists()
        dir.isDirectory()
        git.repository.resolve(Constants.HEAD).name
    }

    def "IsValidGitRepo"() {
        when:
        GitHelper.isValidGitRepo(dir)

        then:
        thrown RepositoryNotFoundException
    }
}
