package com.mreil.gradle.testutil

import org.gradle.api.Action
import org.gradle.api.Task

class TaskHelper {
    static def runTask(Task task) {
        task.actions.each { Action a ->
            a.execute(task)
        }
    }
}
