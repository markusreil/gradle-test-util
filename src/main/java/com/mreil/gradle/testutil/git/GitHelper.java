package com.mreil.gradle.testutil.git;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.File;
import java.io.IOException;

public class GitHelper {
    public static Git initGitAt(File dir) throws GitAPIException {
        if (!dir.exists() || !dir.isDirectory()) {
            throw new IllegalArgumentException(dir.toString() + " is not a directory");
        }

        return Git.init().setDirectory(dir).call();
    }

    public static Git initGitWithInitialCommitAt(File dir) throws GitAPIException {
        Git git = initGitAt(dir);
        git.add().addFilepattern("*.*").call();
        git.commit().setMessage("initial commit").call();
        return git;
    }

    public static boolean isValidGitRepo(File dir) throws IOException {
        String branch = Git.open(dir).getRepository().getBranch();
        return true;
    }

}
