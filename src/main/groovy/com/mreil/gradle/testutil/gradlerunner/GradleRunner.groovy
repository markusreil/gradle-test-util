package com.mreil.gradle.testutil.gradlerunner

import com.mreil.gradle.testutil.git.GitHelper
import org.apache.commons.io.FileUtils
import org.eclipse.jgit.api.Git
import org.gradle.tooling.BuildLauncher
import org.gradle.tooling.GradleConnector
import org.gradle.tooling.ProgressEvent
import org.gradle.tooling.ProgressListener

import java.nio.file.Files

class GradleRunner {
    private BuildLauncher launcher
    File projectDir
    Git git

    GradleRunner(String projectDir) {
        this(projectDir, null)
    }

    GradleRunner(String projectDir,
                 String gradleVersion) {
        def dir = new File(projectDir)
        if (!dir.exists()) {
            throw new IllegalArgumentException("Project directory ${dir.path} does not exist.")
        }

        if (!dir.directory) {
            throw new IllegalArgumentException("${dir.path} is not a directory.")
        }

        if (!new File(dir, "build.gradle")) {
            throw new IllegalArgumentException("no build.gradle file in ${dir}")
        }

        this.projectDir = Files.createTempDirectory("testGradleBuild").toFile()
        FileUtils.copyDirectory(dir, this.projectDir)

        GradleConnector gc = GradleConnector.newConnector()
                .forProjectDirectory(this.projectDir)

        if (gradleVersion != null) {
            gc = gc.useGradleVersion(gradleVersion)
        }

        def project = gc.connect()

        launcher = project.newBuild().withArguments("-i")
    }

    public GradleRunner withArguments(String... arguments) {
        launcher = launcher.withArguments(arguments)
        return this
    }

    public GradleRunner withGit() {
        this.git = GitHelper.initGitWithInitialCommitAt(projectDir)
        return this
    }

    public void run(String... tasks) {
        launcher
                .forTasks(tasks)
                .addProgressListener(new ProgressListener() {
            @Override
            void statusChanged(ProgressEvent progressEvent) {
                println progressEvent
                println progressEvent.description
            }
        }).run()

    }
}
