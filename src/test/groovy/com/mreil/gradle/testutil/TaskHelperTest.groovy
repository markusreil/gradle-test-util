package com.mreil.gradle.testutil

import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class TaskHelperTest extends Specification {
    def "RunTask"() {
        when: "Task is executed"
        Project p = ProjectBuilder.builder().build()
        Task task = p.tasks.create("task", {
            project.file("newfile").write("content")
        })
        TaskHelper.runTask(task)

        then: "The task has run"
        p.file("newfile").exists()
    }
}
